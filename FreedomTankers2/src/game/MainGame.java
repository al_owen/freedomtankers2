package game;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Window;
import clases.Boss;
import clases.Bullets;
import clases.Character;
import clases.Enemies;
import clases.EnemyTanks;
import clases.Spawner;
import enums.Facing;
import enums.StatsInfo;
import environmentalClases.Forest;
import environmentalClases.Hud;
import environmentalClases.Images;
import environmentalClases.Stats;
import maps.Map1;
import maps.Map2;
import maps.Map3;

/**
 * 
 * Freedom_tankers MainGame.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         21 ene. 2020
 */
public class MainGame {

	// timer for control purposes
	static Timer timer = new Timer();
	// random for any purpose
	static Random r = new Random();

	// special mode
	static int modo = 1;

	// selects if we will play or not
	static int selectedLevel = 1;
	
	// creates a Forest variable for the tank to slow down
	static Forest grass;

	// variable to control whether we can fire or not
	static boolean cooldownFire = false;

	// Battleground of the game
	public static Field battleground = new Field();

	// Window where the game takes place
	static Window w = new Window(battleground);

	// Creates the main tank of the player
	static Character player;

	// indicates if the boss is dead or not
	public static boolean deadBoss = false;

	// states the difficulty of the game
	static int difficulty = 1;

	// ************************* The game itself *********************************
	public static void main(String[] args) throws InterruptedException, IOException {

		firecooldown();
		int maxLevels = 3;
		int levelToPlay = menu();
		boolean keepPlaying = true;


		while (keepPlaying && levelToPlay != 0) {
			initmap(levelToPlay);
			Thread.sleep(100);
			boolean won = play();
			if (won && levelToPlay + 1 <= maxLevels) {
				levelToPlay++;
			} else {
				keepPlaying = false;
			}
			if (levelToPlay != 0) {
				Spawner.stopcounter();
			}
		}
		if (deadBoss == true) {
			score();
			battleground.clear();
			battleground.background = "images/menu/win.jpg";
			w.playMusicOnce("sounds/win.wav");
			Thread.sleep(25000);
			System.out.println("You Won");
		} else {
			battleground.clear();
			battleground.background = "images/menu/gameover.jpg";
			w.playMusicOnce("sounds/GameOver.wav");
			Thread.sleep(2000);
			System.out.println("You lose");
		}
		w.close();
		System.exit(0);

	} // *****************************************************************************

	/**
	 * Main menu
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	private static int menu() throws InterruptedException {
		int level = 1;

		boolean play = false;

		Images tank = new Images(600, 500, 660, 560, "images/tanks/yellow.png", battleground);
		tank.angle = 90;
		new Images(700, 500, 1100, 560, "images/menu/level1.png", battleground);
		new Images(700, 600, 1100, 660, "images/menu/exit.png", battleground);
		Stats difficultylevel = new Stats(1400, 550, battleground, StatsInfo.any);
		difficultylevel.updateany("your difficulty level is: "+difficulty);
		Stats special = new Stats(1100, 550, battleground, StatsInfo.any);
		special.updateany(" ");
		battleground.background = "images/menu/menuBackground.png"; // Background for the Main menu
		w.playMusicOnce("sounds/intro.wav"); // original song from Battle city

		while (!play) {

			play = menuInput(tank, play, difficultylevel, special);

			battleground.draw(); // draw map
			Thread.sleep(30); // 30fps

		}
		if (selectedLevel == 1) {
			level = 1;
		} else if (selectedLevel == 0) {
			level = 0;
		}

		battleground.background = null;
		return level;
	}

	/**
	 * play the game
	 * @return
	 * @throws InterruptedException
	 */
	private static boolean play() throws InterruptedException {

		boolean gameOver = false; // condition to keep playing
		boolean haveWon = false;  //checks if the player won

		while (!gameOver) {

			Ongrass();
			input();
			battleground.draw(); // draw map
			Thread.sleep(30);
			gameOver = finish(); // check if the game keeps going

		}
		haveWon = haveWon();
		return haveWon;
	}

	
	/**
	 * initialization of the map 
	 * @param level
	 * @throws InterruptedException
	 */
	private static void initmap(int level) throws InterruptedException {
		battleground.clear(); //clears the map
		cooldownFire = false; 
		battleground.setBackground(Color.DARK_GRAY); // set background color
		
		if (modo == 2) {
			Bullets.image = "images/bullets/roses.png";
		}else if (modo == 3) {
			Character.image = "images/tanks/enemy.gif";
			EnemyTanks.image="images/tanks/amerikan.gif";
			Boss.image = "images/tanks/joan.jpg";
		}
		
		if (level == 1) {
			//plays music
			w.playMusic("sounds/backgroundsound.wav");
			//sets the forest
			grass = new Forest(460, 250, 1260, 760, "images/enviroment/grassx.png", battleground);
			//sets the player
			player = Character.getInstance(300, 470, 360, 530, Facing.RIGHT, 25, 100, 7, battleground);

			// map setup
			new Spawner(battleground, 1);
			Spawner.enemies = 3 * difficulty; //increases the quantity of enemies that needs to be defeated
			Spawner.maxSpawn += difficulty; //increases the quantity of enemies that can spawn
			Map1.terrain(battleground);
			Map1.river(battleground);
			Map1.walls(battleground);

			// start of the timer
			Enemies.firecooldown();
			//starts the hud
			Hud.gethud(1710, 50, battleground);

		} else if (level == 2) {
			//plays music
			w.playMusic("sounds/backgroundsound.wav");
			//sets the forest
			grass = new Forest(460, 250, 1260, 760, "images/enviroment/grassx.png", battleground);
			//sets the player
			player = Character.getInstance(90, 910, 150, 970, Facing.UP, 25, 100, 7, battleground);

			// map2 setup
			new Spawner(battleground, 2);
			Spawner.enemies = 4 * difficulty; //increases the quantity of enemies that needs to be defeated
			Spawner.maxSpawn += difficulty; //increases the quantity of enemies that can spawn
			Map2.terrain(battleground);
			Map2.walls(battleground);

			// start of the timer
			Enemies.firecooldown();
			//refreshes the hud
			Hud.restarthud(battleground);

		} else if (level == 3) {
			//plays music
			w.playMusic("sounds/boss.wav");
			//sets the forest
			grass = new Forest(100, 250, 800, 760, "images/enviroment/grassx.png", battleground);
			//sets the player
			player = Character.getInstance(90, 910, 150, 970, Facing.UP, 25, 100, 7, battleground);

			// map3 setup
			new Spawner(battleground, 3);
			Spawner.enemies = 30 * difficulty; //increases the quantity of enemies that needs to be defeated
			Spawner.maxSpawn += difficulty; //increases the quantity of enemies that can spawn
			Map3.terrain(battleground);
			Map3.river(battleground);
			Map3.boss(battleground, difficulty);

			// start of the timer
			Enemies.firecooldown();
			//refreshes the hud
			Hud.restarthud(battleground);
		}

	}

	
	//////////////////////////////////////// timer
	//////////////////////////////////////// /////////////////////////////////////////////////////////////////////

	/**
	 * cooldown for the bullets
	 */
	private static void firecooldown() {

		TimerTask cooldown = new TimerTask() {

			@Override
			public void run() {
				if (!cooldownFire) {
					cooldownFire = true;
				}
			}
		};
		timer.schedule(cooldown, 0, 800);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////// special checks
	//////////////////////////////////////////////////////// ////////////////////////////////////////////////////////
	/**
	 * checks whether the player is stepping on grass
	 * 
	 * @param grass
	 */
	private static void Ongrass() {
		player.forest(grass);
	}

	/**
	 * checks whether the game is over or not
	 * 
	 * @param gameOver
	 * @return
	 */
	private static boolean finish() {
		return Character.getInstance().hp < 1 || Spawner.enemies < 1 || deadBoss;
	}

	/**
	 * If enemies are 0 returns true
	 * 
	 * @return
	 */
	private static boolean haveWon() {
		return Spawner.enemies < 1 || deadBoss;
	}
	
	/**
	 * stores the score
	 * @throws IOException
	 */
	private static void score() throws IOException {
		BufferedWriter scorew = new BufferedWriter(new FileWriter("doc/scores/scores.txt", true));
		
		scorew.write("Last score was: "+player.xp);
		scorew.newLine();
		scorew.flush();
		scorew.close();
		
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////////////////// inputs
	//////////////////////////////////////////////////////////////////// /////////////////////////////////////////////////////////

	/**
	 * Input for the main menu
	 * 
	 * @param tank
	 * @param play
	 * @param difficultylevel 
	 * @param special 
	 * @return
	 * @throws InterruptedException
	 */
	private static boolean menuInput(Images tank, boolean play, Stats difficultylevel, Stats special) throws InterruptedException {
		// checks whether we can press the button again or not
		if (cooldownFire) {
			if (w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W')) { // goes up
				if (tank.y1 == 600 && tank.y2 == 660) {
					tank.y1 = 500;
					tank.y2 = 560;
					selectedLevel = 1;
					cooldownFire = false;
				}
			}
			if (w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S')) { // goes down
				if (tank.y1 == 500 && tank.y2 == 560) {
					tank.y1 = 600;
					tank.y2 = 660;
					selectedLevel = 0;
					cooldownFire = false;
				}

			}
			if (w.getPressedKeys().contains('+')) { // harder
				if (difficulty < 10) {
					difficulty++;
					difficultylevel.updateany("your difficulty level is: "+difficulty);
					cooldownFire = false;
				}
			}
			if (w.getPressedKeys().contains('-')) { // easier
				if (difficulty >= 2) {
					difficulty--;
					difficultylevel.updateany("your difficulty level is: "+difficulty);
					cooldownFire = false;
				}
			}
			if (w.getPressedKeys().contains('.')) { // easier
				if (modo > 0 && modo <= 3) {
					modo++;
					if (modo == 2) {
						special.updateany("modo empar");
					}else if (modo == 3) {
						special.updateany("modo joan");
					}else if (modo == 4) {
						modo = 1;
						special.updateany(" ");
					}
					
					cooldownFire = false;
				}
			}
			if (w.getPressedKeys().contains('e') || w.getPressedKeys().contains('E')) { // selects the option
				play = true;
			}
		}
		return play;
	}

	/**
	 * checks which key is being pressed
	 * 
	 * @throws InterruptedException
	 */
	private static void input() throws InterruptedException {

		if (!w.getPressedKeys().contains('w') && !w.getPressedKeys().contains('W') && !w.getPressedKeys().contains('d')
				&& !w.getPressedKeys().contains('D') && !w.getPressedKeys().contains('s')
				&& !w.getPressedKeys().contains('S') && !w.getPressedKeys().contains('a')
				&& !w.getPressedKeys().contains('A')) {
			player.stopmoving(player);
		} else {
			if (w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D')) {
				player.moveright(player);
			}
			if (w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W')) {
				player.moveup(player);
			}
			if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A')) {
				player.moveleft(player);
			}
			if (w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S')) {
				player.movedown(player);
			}
		}

		fire();

	}

	/**
	 * shoots the bullet
	 * 
	 * @throws InterruptedException
	 */
	private static void fire() throws InterruptedException {

		if (cooldownFire) {
			if (w.getKeysDown().contains(' ')) {
				w.playSFX("sounds/shot.wav");
				player.shooting();
				cooldownFire = false;
			}

		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
