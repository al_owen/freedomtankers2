package interfaces;

import clases.Bullets;

/**
 * interface for destructible objects
 * 
 * Freedom_tankers Destructible.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         24 abr. 2020
 */
public interface Destructible {

	/**
	 * method that sets how bullet damage affects this object
	 * @param bullets 
	 * @param bullets 
	 */
	public void destroy(Bullets bullets);


}
