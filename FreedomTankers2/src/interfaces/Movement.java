package interfaces;

import clases.Tanks;
import enums.Facing;

/**
 * Freedom_tankers Movement.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         25 mar. 2020
 */
public interface Movement {

	/**
	 * function that allows the tank to move up
	 * 
	 * @param tank
	 */
	public default void moveup(Tanks t) {
		t.setVelocity(0, -t.speed);
		t.rotate(Facing.UP);
	}

	/**
	 * function that allows the tank to move down
	 * 
	 * @param tank
	 */
	public default void movedown(Tanks t) {
		t.setVelocity(0, t.speed);
		t.rotate(Facing.DOWN);
	}

	/**
	 * function that allows the tank to move left
	 * 
	 * @param tank
	 */
	public default void moveleft(Tanks t) {
		t.setVelocity(-t.speed, 0);
		t.rotate(Facing.LEFT);
	}

	/**
	 * function that allows the tank to move right
	 * 
	 * @param tank
	 */
	public default void moveright(Tanks t) {
		t.setVelocity(t.speed, 0);
		t.rotate(Facing.RIGHT);
	}

	/**
	 * function that stops the tank
	 * 
	 * @param tank
	 */
	public default void stopmoving(Tanks t) {
		t.setVelocity(0, 0);

	}

}
