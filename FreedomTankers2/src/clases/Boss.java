package clases;

import Core.Field;
import Core.Sprite;
import enums.Facing;
import environmentalClases.Forest;
import environmentalClases.Hud;
import game.MainGame;

/**
 * FreedomTankers2 Boss.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         27 may. 2020
 */
public class Boss extends Enemies {
	
	public static String image = "images/tanks/boss.png";

	/**
	 * 
	 * @param x1         The horizontal position in the field of the upper-left
	 *                   pixel in the sprite
	 * @param y1         The vertical position in the field of the upper-left pixel
	 *                   in the sprite
	 * @param x2         The horizontal position in the field of the bottom-right
	 *                   pixel in the sprite
	 * @param y2         The vertical position in the field of the bottom-right
	 *                   pixel in the sprite
	 * @param f          field where the boss will be draw
	 * @param difficulty sets the difficulty of the boss
	 */
	public Boss(int x1, int y1, int x2, int y2, Field f, int difficulty) {
		super("Nage Fahrer", x1, y1, x2, y2, Facing.DOWN, image, 40, 100 * difficulty, 8, f, 500);
	}

	@Override
	public void update() {
		if (this.y1<50) {
			movedown(this);
		}else if (this.y2 > 970) {
			moveup(this);
		}
		
		if (Enemies.cooldownFire) {
			if ((((this.y1 + this.y2) / 2) > Character.getInstance().y1
					&& ((this.y1 + this.y2) / 2) < Character.getInstance().y2)) {
				this.rotate(Facing.LEFT);
				this.shooting();
			}
		}
	}

	@Override
	public void destroy(Bullets b) {
		if (b.friendly) {
			if (this.hp > 0) {
				this.hp -= b.damage;
			} else {
				Character.getInstance().xp += this.xp;
				Hud.gethud().xp.updatexp();
				MainGame.deadBoss=true;
				this.delete();
			}
			b.delete();
		}

	}

	@Override
	public void forest(Forest grass) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionStay(Sprite sp) {


	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionEnter(Sprite sp) {

		
	}

}
