package clases;

import java.util.Random;

import Core.Field;
import Core.Sprite;
import enums.Facing;
import environmentalClases.Forest;
import environmentalClases.Hud;
import environmentalClases.Perks;
import interfaces.Destructible;
import interfaces.Movement;

public class Character extends Tanks implements Movement, Destructible {

	Random r = new Random();

	public static String image = "images/tanks/amerikan.gif";
	
	// adds experience points to the tank
	public int xp;

	// creates a singleton character
	private static Character instance = null;

	/**
	 * private constructor for the main character
	 * 
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle with sides
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 * @param f      field where the tank will be draw
	 */
	private Character(int x1, int y1, int x2, int y2, Facing angle, int attack, int hp, int speed, Field f) {
		super("amerikan", x1, y1, x2, y2, angle, image, attack, hp, speed, f, true);
		this.xp = 0;
		this.orderInLayer = 1;
	}
	/**
	 * method for the creation of a Singleton character
	 * 
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle with sides
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 * @param f      field where the tank will be draw
	 * 
	 * * @return instance of the character
	 */
	public static Character getInstance(int x1, int y1, int x2, int y2, Facing angle, int attack, int hp, int speed,
			Field f) {
		if (instance == null) {
			instance = new Character(x1, y1, x2, y2, angle, attack, hp, speed, f);
		} else {
			setInstance(x1, y1, x2, y2, angle, attack, hp, speed, f);
		}
		return instance;
	}

	/**
	 * returns the Character
	 * @return
	 */
	public static Character getInstance() {
		return instance;
	}

	/**
	 * updates the character
	 * 
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle with sides
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 * @param f      field where the tank will be draw
	 */
	private static void setInstance(int x1, int y1, int x2, int y2, Facing angle, int attack, int hp, int speed,
			Field f) {
		instance.f = f;
		instance.x1 = x1;
		instance.y1 = y1;
		instance.x2 = x2;
		instance.y2 = y2;
		instance.side = angle;
		instance.attack = attack;
		instance.hp = hp;
		instance.speed = speed;
		f.sprites.add(instance);
		Hud.gethud().health.updatehp();
	}

	@Override
	public void forest(Forest grass) {
		if (this.collidesWith(grass)) {
			this.speed = this.lastSpeed / 2;
			Hud.gethud().speed.updatespeed();
		} else {
			this.speed = this.lastSpeed;
			Hud.gethud().speed.updatespeed();
		}

	}

	@Override
	public void destroy(Bullets b) {
		if (!b.friendly) {
			b.delete();
			if (hp > 0) {
				this.hp -= b.damage;
				Hud.gethud().health.updatehp();
			}
		}
	}

	@Override
	public String toString() {
		return "Character [xp=" + xp + ", r=" + r + ", speed=" + speed + ", hp=" + hp + ", attack=" + attack
				+ ", friendly=" + friendly + ", name=" + name + ", x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2="
				+ y2 + "]";
	}

	@Override
	public void onTriggerEnter(Sprite sp) {
		// System.out.println(sp.name);
		if (sp instanceof Perks) {
			((Perks) sp).effect(this);
			sp.delete();
		}
	}

	@Override
	public void onCollisionStay(Sprite sp) {

	}

	@Override
	public void onCollisionExit(Sprite sp) {

	}

	@Override
	public void onCollisionEnter(Sprite sprite) {

	}
}
