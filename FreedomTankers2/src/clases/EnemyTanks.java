
package clases;

import Core.Field;
import Core.Sprite;
import enums.Facing;
import environmentalClases.Borders;
import environmentalClases.Forest;
import environmentalClases.Hud;
import environmentalClases.MetalWall;
import environmentalClases.Walls;
import environmentalClases.Water;

public class EnemyTanks extends Enemies{
	
	public static String image = "images/tanks/enemy.gif";

	/**
	 * Constructor of the EnemyTanks class
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 */
	public EnemyTanks(String name, int x1, int y1, int x2, int y2, Facing angle, int attack, int hp,
			Field f) {
		super(name, x1, y1, x2, y2, angle, image, attack, hp, 2, f, 25);
		if (this.firstCollidesWithField() instanceof EnemyTanks) {
			Spawner.spawned--;
			this.delete();
		}
	}

	@Override
	public void forest(Forest grass) {
		if (this.collidesWith(grass)) {
			this.speed = this.lastSpeed++;
		} else {
			this.speed = this.lastSpeed;
		}

	}

	@Override
	public void destroy(Bullets b) {
		if (b.friendly) {
			if (this.hp > 0) {
				this.hp -= b.damage;
			} else {
				Spawner.spawned--;
				Spawner.enemies--;
				Character.getInstance().xp += this.xp;
				Hud.gethud().xp.updatexp();
				Hud.gethud().enemies.updateenemies();
				this.delete();
			}
			b.delete();
		}

	}
	
	@Override
	public String toString() {
		return "EnemyTanks [speed=" + speed + ", hp=" + hp + ", attack=" + attack + ", x1=" + x1 + ", y1=" + y1
				+ ", x2=" + x2 + ", y2=" + y2 + "]";
	}


	@Override
	public void onCollisionEnter(Sprite sp) {
		if (sp instanceof Character) {
			((Character) sp).hp -= this.attack;
			((Character) sp).xp -= this.xp/2;
			Spawner.spawned--;
			Spawner.enemies--;
			Hud.gethud().health.updatehp();
			Hud.gethud().enemies.updateenemies();
			this.delete();
		}
		if (sp instanceof MetalWall || sp instanceof Water) {
			this.moveleft(this);
		}else if (sp instanceof Borders) {
			this.moveright(this);
		}else if (sp instanceof EnemyTanks || sp instanceof Walls) {
			changeway();
		}
	}


	@Override
	public void onCollisionExit(Sprite sp) {

	}
	
	@Override
	public void onCollisionStay(Sprite sp) {

	}
	
	/**
	 * checks where the tank is going and moves the to the opposite direction
	 */
	private void changeway() {
		if (this.side == Facing.RIGHT) {
			this.moveleft(this);
		}else if (this.side == Facing.LEFT) {
			this.moveright(this);
		}else if (this.side == Facing.UP) {
			this.movedown(this);
		}else if (this.side == Facing.DOWN) {
			this.moveup(this);
		}
		
	}
}
