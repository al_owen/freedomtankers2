package clases;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import Core.Field;
import environmentalClases.Perks;
import maps.Map1;
import maps.Map2;
import maps.Map3;

/**
 * FreedomTankers2 Spawner.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         17 may. 2020
 */
public class Spawner  {
	
	static Random r = new Random();

	static Timer timer = new Timer();
	
	static Field f;
	
	//creates a timer task to generate enemies
	static TimerTask enemygen;
	
	//creates a timer task to generate perks
	static TimerTask perkgen;
	
	/**
	 * indicates how many enemies we need to destroy in order to win
	 */
	public static int enemies;

	/**
	 * indicates the level we are playing
	 */
	public static int level;

	/**
	 * indicates how many enemy tanks can spawn at the same time
	 */
	public static int maxSpawn;

	/**
	 * checks how many tanks have spawned
	 */
	public static int spawned;


	/**
	 * constructor of the spawner
	 * @param f
	 * @param level
	 */
	public Spawner(Field f, int level) {
		super();
		Spawner.f = f;
		Spawner.spawned = 0;
		Spawner.maxSpawn = 3;
		Spawner.level = level;
		enemygen = new TimerTask() {
			
			@Override
			public void run() {
				generateEnemies();
				
			}
		};
		
		perkgen = new TimerTask() {

			@Override
			public void run() {
				generatePerks();
				
				
			}

		};
		timer.schedule(enemygen, 2000, 10000);
		timer.schedule(perkgen, 10000, 30000);
	}

	/**
	 * generates perks
	 */
	protected void generatePerks() {
		if (level == 1) {
			int rnum = r.nextInt(4)+1;
			if (rnum == 1) {
				new Perks(600, 450, 650, 500, f);
			}else if(rnum == 2) {
				new Perks(1430, 70, 1480, 120, f);
			}else if(rnum == 3) {
				new Perks(1430, 900, 1480, 950, f);
			}
		}else if (level == 2) {
			int rnum = r.nextInt(4)+1;
			if (rnum == 1) {
				new Perks(600, 450, 650, 500, f);
			}else if(rnum == 2) {
				new Perks(1130, 70, 1180, 120, f);
			}else if(rnum == 3) {
				new Perks(1130, 900, 1180, 950, f);
			}
		}else if (level == 3) {
			int rnum = r.nextInt(3)+1;
			if (rnum == 1) {
				new Perks(600, 450, 650, 500, f);
			}
		}
		
	}

	/**
	 * generates enemies
	 */
	protected static void generateEnemies() {
		if (level == 1) {
			Map1.enemies(f);
		} else if (level == 2) {
			Map2.enemies(f);
		} else if(level == 3) {
			Map3.enemies(f);
		}

	}
	
	/**
	 * stops all counters
	 */
	public static void stopcounter() {
		perkgen.cancel();
		enemygen.cancel();

	}

}
