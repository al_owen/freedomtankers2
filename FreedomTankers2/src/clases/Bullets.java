package clases;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import interfaces.Destructible;

public class Bullets extends PhysicBody {

	public static String image = "images/bullets/bullets.png";
	
	// bullets special variables
	public int bspeed;
	public boolean friendly;
	int damage;

	/**
	 * This is the Bullet class, it creates bullets that deals damage and can be
	 * fired by tanks
	 * 
	 * @param name     sets the name of the sprite
	 * @param x1       The horizontal position in the field of the upper-left pixel
	 *                 in the sprite
	 * @param y1       The vertical position in the field of the upper-left pixel in
	 *                 the sprite
	 * @param x2       The horizontal position in the field of the bottom-right
	 *                 pixel in the sprite
	 * @param y2       The vertical position in the field of the bottom-right pixel
	 *                 in the sprite
	 * @param angle    The rotating angle in degrees
	 * @param path     asks for the path to the image to that Sprite
	 * @param friendly checks if the bullet was fired by the player or by an enemy
	 * @param bspeed   sets the speed at which the bullets will move
	 * @param damage   damage dealt by the bullet
	 * @param f        field where the boss will be draw
	 */
	public Bullets(String name, int x1, int y1, int x2, int y2, double angle, boolean friendly, int bspeed,
			int damage, Field f) {
		super(name, x1, y1, x2, y2, angle, image, f);
		this.trigger = true;
		this.friendly = friendly;
		this.bspeed = bspeed;
		this.damage = damage;
	}

	@Override
	public void onCollisionEnter(Sprite sp) {
		if (sp instanceof Destructible) {
			((Destructible) sp).destroy(this);
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
