package clases;

import Core.Field;
import Core.PhysicBody;
import enums.Facing;
import environmentalClases.Forest;
import interfaces.Movement;

public abstract class Tanks extends PhysicBody implements Movement {

	// speed at which the tank moves
	public int speed;

	// indicates where the tank is facing
	public Facing side;

	// speed at which the bullets moves
	public int bspeed;

	// saves the last speed in case the speed is modified
	public int lastSpeed;

	// health points of the tank
	public int hp;

	// damage points that the tank deals
	public int attack;

	// indicates whether the tank is friendly or not
	public boolean friendly;

	/**
	 * /** Constructor of the class tank
	 * 
	 * @param name     sets the name of the sprite
	 * @param x1       The horizontal position in the field of the upper-left pixel
	 *                 in the sprite
	 * @param y1       The vertical position in the field of the upper-left pixel in
	 *                 the sprite
	 * @param x2       The horizontal position in the field of the bottom-right
	 *                 pixel in the sprite
	 * @param y2       The vertical position in the field of the bottom-right pixel
	 *                 in the sprite
	 * @param path     asks for the path to the image to that Sprite
	 * @param angle    The rotating angle in degrees
	 * @param attack   sets the damage that the tank deals in every shot
	 * @param hp       sets the health of the player's tank
	 * @param speed    sets the speed at which the tank will move
	 * @param f        field where the tank will be draw
	 * @param friendly indicates whether the tank is friendly or not
	 */
	public Tanks(String name, int x1, int y1, int x2, int y2, Facing side, String path, int attack, int hp, int speed,
			Field f, boolean friendly) {
		super(name, x1, y1, x2, y2, side(side), path, f);
		this.attack = attack;
		this.hp = hp;
		this.side = side;
		this.speed = speed;
		this.lastSpeed = speed;
		this.bspeed = speed + 2;
		this.friendly = friendly;

	}

	/**
	 * indicates the rotation based on where the tank is facing
	 * @param angle
	 * @return
	 */
	private static double side(Facing angle) {
		if (angle == Facing.UP) {
			return 0;
		} else if (angle == Facing.RIGHT) {
			return 90;
		} else if (angle == Facing.DOWN) {
			return 180;
		} else if (angle == Facing.LEFT) {
			return 270;
		}
		return 0;

	}

	/**
	 * rotates the tank based on where the tank is facing
	 * @param angle
	 */
	public void rotate(Facing angle) {
		if (angle == Facing.UP) {
			this.angle = 0;
			this.side = Facing.UP;
		} else if (angle == Facing.RIGHT) {
			this.angle = 90;
			this.side = Facing.RIGHT;
		} else if (angle == Facing.DOWN) {
			this.angle = 180;
			this.side = Facing.DOWN;
		} else if (angle == Facing.LEFT) {
			this.angle = 270;
			this.side = Facing.LEFT;
		}
	}

	/**
	 * @return the lastSpeed
	 */
	public int getLastSpeed() {
		return lastSpeed;
	}

	/**
	 * @param sets the lastSpeed
	 */
	public void setLastSpeed(int lastSpeed) {
		this.lastSpeed = lastSpeed;
	}

	/**
	 * shoots
	 */
	public void shooting() {
		if (this.side == Facing.UP) {
			Bullets bup = new Bullets("bang", (x1 + x2) / 2 - 25, (y1 + y2) / 2 - 5, (x1 + x2) / 2 + 25,
					(y1 + y2) / 2 + 5, 270, friendly, 9, attack, f);
			bup.setVelocity(0, -bup.bspeed);
			if (this instanceof Enemies) {
				Enemies.cooldownFire = false;
			}
		} else if (side == Facing.RIGHT) {
			Bullets br = new Bullets("bang", (x1 + x2) / 2 - 25, (y1 + y2) / 2 - 5, (x1 + x2) / 2 + 25,
					(y1 + y2) / 2 + 5, 0, friendly, 9, attack, f);
			br.setVelocity(br.bspeed, 0);
			if (this instanceof Enemies) {
				Enemies.cooldownFire = false;
			}
		} else if (side == Facing.DOWN) {
			Bullets bd = new Bullets("bang", (x1 + x2) / 2 - 25, (y1 + y2) / 2 - 5, (x1 + x2) / 2 + 25,
					(y1 + y2) / 2 + 5, 90, friendly, 9, attack, f);
			bd.setVelocity(0, bd.bspeed);
			if (this instanceof Enemies) {
				Enemies.cooldownFire = false;
			}
		} else if (side == Facing.LEFT) {
			Bullets bl = new Bullets("bang", (x1 + x2) / 2 - 25, (y1 + y2) / 2 - 5, (x1 + x2) / 2 + 25,
					(y1 + y2) / 2 + 5, 180, friendly, 9, attack, f);
			bl.setVelocity(-bl.bspeed, 0);
			if (this instanceof Enemies) {
				Enemies.cooldownFire = false;
			}
		}
	}


	/**
	 * checks if a tanks has collided with the grass and reduces the speed
	 * accordingly
	 * 
	 * @param grass
	 */
	public abstract void forest(Forest grass);

}
