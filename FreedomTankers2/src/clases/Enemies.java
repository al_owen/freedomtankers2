package clases;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import Core.Field;
import enums.Facing;
import interfaces.Destructible;

/**
 * Freedom_tankers Enemies.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         20 abr. 2020
 */
public abstract class Enemies extends Tanks implements Destructible {

	public static boolean cooldownFire = false;

	public int xp;

	static Timer timer = new Timer();
	static Random r = new Random();

	/**
	 * Default constructor for the abstract enemy class
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 */
	public Enemies(String name, int x1, int y1, int x2, int y2, Facing angle, String path, int attack, int hp,
			int speed, Field f, int xp) {
		super(name, x1, y1, x2, y2, angle, path, attack, hp, speed, f, false);
		this.xp = xp;
		this.defaultmovement(angle);
	}

	
	/**
	 * sets a default movement for the enemy
	 * @param angle
	 */
	private void defaultmovement(Facing angle) {
		if (angle == Facing.UP) {
			this.setVelocity(0, -this.speed);
		} else if (angle == Facing.LEFT) {
			this.setVelocity(-this.speed, 0);
		} else if (angle == Facing.DOWN) {
			this.setVelocity(0, this.speed);
		} else if (angle == Facing.RIGHT) {
			this.setVelocity(this.speed, 0);
		}

	}

	
	@Override
	public void update() {
		if (Enemies.cooldownFire) {
			if ((((this.x1 + this.x2) / 2) > Character.getInstance().x1
					&& ((this.x1 + this.x2) / 2) < Character.getInstance().x2)
					|| (((this.y1 + this.y2) / 2) > Character.getInstance().y1
							&& ((this.y1 + this.y2) / 2) < Character.getInstance().y2)) {
				if (this.y2 <= Character.getInstance().y1) {
					this.movedown(this);
				} else if (this.y1 >= Character.getInstance().y2) {
					this.moveup(this);
				} else if (this.x2 <= Character.getInstance().x1) {
					this.moveright(this);
				} else if (this.x1 >= Character.getInstance().x2) {
					this.moveleft(this);
				}
				this.shooting();
			}
		}
	}

	/**
	 * sets a fire cooldown for the enemies
	 */
	public static void firecooldown() {

		TimerTask cooldown = new TimerTask() {

			@Override
			public void run() {
				if (!cooldownFire) {
					cooldownFire = true;
				}
			}
		};
		timer.schedule(cooldown, 0, 3000);
	}

}
