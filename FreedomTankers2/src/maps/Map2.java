package maps;

import java.util.Random;

import Core.Field;
import clases.EnemyTanks;
import clases.Spawner;
import enums.Facing;
import environmentalClases.Borders;
import environmentalClases.Walls;

/**
 * Map generator for the second level
 * 
 * Freedom_tankers Map2.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         24 abr. 2020
 */
public class Map2 {
	
	static Random r = new Random();

	/**
	 * creates the limits of the map and the non-destructible
	 * 
	 * @param f field for the sprites to be drawn
	 * 
	 */
	public static void terrain(Field f) {
		// limit of the map vertical
		new Borders("limit1", 0, 0, 30, 1020, "images/enviroment/limit.png", f);
		new Borders("limit2", 1670, 0, 1700, 1020, "images/enviroment/limit.png", f);
		// limit of the map horizontal
		new Borders("limit3", 30, 0, 1700, 30, "images/enviroment/limitup.png", f);
		new Borders("limit4", 30, 990, 1700, 1020, "images/enviroment/limitup.png", f);

		// metalwall
		new Borders("midleft", 280, 30, 320, 400, "images/enviroment/verticalMetal.png", f);
		new Borders("midright", 1430, 30, 1470, 400, "images/enviroment/verticalMetal.png", f);
		new Borders("midup", 670, 280, 1070, 320, "images/enviroment/horizontalMetal.png", f);
		new Borders("middown", 670, 700, 1070, 740, "images/enviroment/horizontalMetal.png", f);

	}


	/**
	 * creates the brick walls that can destroyed
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void walls(Field f) {
		// every brick wall is 40 x 40

		int y1 = 610;

		// these are the bottom bricks
		for (int i = 0; i < 9; i++) {
			new Walls(220, y1, 260, y1 + 40, f);
			new Walls(260, y1, 300, y1 + 40, f);
			new Walls(300, y1, 340, y1 + 40, f);
			new Walls(1390, y1, 1430, y1 + 40, f);
			new Walls(1430, y1, 1470, y1 + 40, f);
			new Walls(1470, y1, 1510, y1 + 40, f);
			y1 += 40;
		}

		// these are the middle bricks top-down
		y1 = 360;
		for (int i = 0; i < 7; i++) {
			new Walls(720, y1, 760, y1 + 40, f);
			new Walls(760, y1, 800, y1 + 40, f);
			new Walls(920, y1, 960, y1 + 40, f);
			new Walls(960, y1, 1000, y1 + 40, f);
			y1 += 40;
		}

		// these are the middle bricks left-right
		int x1 = 800;
		for (int i = 0; i < 3; i++) {
			new Walls(x1, 360, x1 + 40, 400, f);
			new Walls(x1, 400, x1 + 40, 440, f);
			new Walls(x1, 560, x1 + 40, 600, f);
			new Walls(x1, 600, x1 + 40, 640, f);
			x1 += 40;
		}
	}

	/**
	 * Creates the enemies of this map
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void enemies(Field f) {
		int position = r.nextInt(4)+1;
		
		if (Spawner.spawned < Spawner.maxSpawn) {
			switch (position) {
			case 1:
				new EnemyTanks("enemy", 430, 900, 490, 960, Facing.UP, 20, 100, f);
				Spawner.spawned++;
				break;
			case 2:
				new EnemyTanks("enemy2", 430, 50, 490, 110, Facing.DOWN, 20, 100, f);
				Spawner.spawned++;
				break;
			case 3:
				new EnemyTanks("enemy3", 1270, 900, 1330, 960, Facing.UP, 20, 100, f);
				Spawner.spawned++;
				break;
			case 4:
				new EnemyTanks("enemy4", 1270, 50, 1330, 110, Facing.DOWN, 20, 100, f);
				Spawner.spawned++;
				break;
			default:
				break;
			}
		}

	}

}
