package maps;

import java.util.Random;

import Core.Field;
import clases.EnemyTanks;
import clases.Spawner;
import enums.Facing;
import environmentalClases.Borders;
import environmentalClases.MetalWall;
import environmentalClases.Walls;
import environmentalClases.Water;

/**
 * Map generator for the first level
 * 
 * Freedom_tankers Map1.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         23 abr. 2020
 */
public class Map1 {

	static Random r = new Random();

	/**
	 * creates the limits of the map and the non-destructible
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void terrain(Field f) {

		// limit of the map vertical
		new Borders("limit1", 0, 0, 30, 1020, "images/enviroment/limit.png", f);
		new Borders("limit2", 1670, 0, 1700, 1020, "images/enviroment/limit.png", f);
		// limit of the map horizontal
		new Borders("limit3", 30, 0, 1700, 30, "images/enviroment/limitup.png", f);
		new Borders("limit4", 30, 990, 1700, 1020, "images/enviroment/limitup.png", f);

		// player side
		new MetalWall("up", 30, 330, 310, 370, 0, "images/enviroment/horizontalMetal.png", f);
		new MetalWall("down", 30, 640, 310, 680, 0, "images/enviroment/horizontalMetal.png", f);

		// enemy side
		new MetalWall("up", 1520, 30, 1560, 300, 0, "images/enviroment/verticalMetals.png", f);
		new MetalWall("middle", 1520, 700, 1560, 990, 0, "images/enviroment/verticalMetals.png", f);
		new MetalWall("down", 1270, 480, 1670, 520, 0, "images/enviroment/horizontalMetal.png", f);

		// middle
		new MetalWall("midup", 740, 210, 1140, 250, 0, "images/enviroment/horizontalMetal.png", f);
		new MetalWall("middown", 740, 760, 1140, 800, 0, "images/enviroment/horizontalMetal.png", f);

	}

	/**
	 * creates the water elements of the map
	 * @param f field for the sprites to be drawn
	 */
	public static void river(Field f) {
		// middle river
		new Water("water", 460, 130, 740, 250, 0, "images/enviroment/river.gif", f); // left
		new Water("water", 460, 760, 740, 880, 0, "images/enviroment/river.gif", f); // right

	}

	/**
	 * creates the brick walls that can destroyed
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void walls(Field f) {
		// every brick wall is 40 x 40
		int x1 = 310;
		int y1 = 0;
		for (int i = 0; i < 24; i++) {
			new Walls(x1, 330, x1 + 40, 370, f);
			new Walls(x1, 640, x1 + 40, 680, f);

			new Walls(x1 + 150, 250, x1 + 190, 290, f);
			new Walls(x1 + 150, 290, x1 + 190, 330, f);

			new Walls(x1 + 150, 680, x1 + 190, 720, f);
			new Walls(x1 + 150, 720, x1 + 190, 760, f);
			x1 += 40;
		}
		y1 = 365;
		for (int i = 0; i < 7; i++) {
			new Walls(430, y1, 470, y1 + 40, f);
			new Walls(470, y1, 510, y1 + 40, f);
			new Walls(510, y1, 550, y1 + 40, f);
			new Walls(550, y1, 590, y1 + 40, f);
			y1 += 40;
		}
		x1 = 740;
		for (int i = 0; i < 8; i++) {
			int y1u = 40;
			int y1d = 810;
			for (int j = 0; j < 4; j++) {
				new Walls(x1, y1u, x1 + 40, y1u + 40, f);
				new Walls(x1, y1d, x1 + 40, y1d + 40, f);
				y1u += 40;
				y1d += 40;
			}
			x1 += 40;
		}
	}

	/**
	 * Creates the enemies of this map
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void enemies(Field f) {
		int position = r.nextInt(4) + 1;
		if (Spawner.spawned < Spawner.maxSpawn) {
			switch (position) {
			case 1:
				new EnemyTanks("enemy1", 80, 40, 140, 100, Facing.RIGHT, 20, 100, f);
				Spawner.spawned++;
				break;
			case 2:
				new EnemyTanks("enemy2", 80, 900, 140, 960, Facing.RIGHT, 20, 100, f);
				Spawner.spawned++;
				break;
			case 3:
				new EnemyTanks("enemy3", 1580, 900, 1640, 960, Facing.UP, 20, 100, f);
				Spawner.spawned++;
				break;
			case 4:
				new EnemyTanks("enemy4", 1580, 50, 1640, 110, Facing.DOWN, 20, 100, f);
				Spawner.spawned++;
				break;
			default:
				break;
			}
		}

	}

}
