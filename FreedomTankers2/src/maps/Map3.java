package maps;

import java.util.Random;

import Core.Field;
import clases.Boss;
import clases.EnemyTanks;
import clases.Spawner;
import enums.Facing;
import environmentalClases.Borders;
import environmentalClases.Water;

/**
 * FreedomTankers2
 * Map3.java
 * @author Alvaro Owen de la Quintana
 *
 * 27 may. 2020
 */
public class Map3 {
	
	static Random r = new Random();

	/**
	 * creates the limits of the map and the non-destructible
	 * 
	 * @param f field for the sprites to be drawn
	 * 
	 */
	public static void terrain(Field f) {
		// limit of the map vertical
		new Borders("limit1", 0, 0, 30, 1020, "images/enviroment/limit.png", f);
		new Borders("limit2", 1670, 0, 1700, 1020, "images/enviroment/limit.png", f);
		// limit of the map horizontal
		new Borders("limit3", 30, 0, 1700, 30, "images/enviroment/limitup.png", f);
		new Borders("limit4", 30, 990, 1700, 1020, "images/enviroment/limitup.png", f);
		
	}

	/**
	 * creates the water elements of the map
	 * @param f field for the sprites to be drawn
	 */
	public static void river(Field f) {
		new Water("water", 800, 30, 950, 990, 0, "images/enviroment/longriver.gif", f);
		
		
	}

	
	/**
	 * creates the boss
	 * @param f field for the sprites to be drawn
	 * @param difficulty
	 */
	public static void boss(Field f, int difficulty) {
		new Boss(1200, 200, 1400, 400, f, difficulty);
		
	}

	/**
	 * Creates the enemies of this map
	 * 
	 * @param f field for the sprites to be drawn
	 */
	public static void enemies(Field f) {
		int position = r.nextInt(2) + 1;
		
		if (Spawner.spawned < Spawner.maxSpawn) {
			switch (position) {
			case 1:
				new EnemyTanks("enemy", 500, 50, 560, 110, Facing.DOWN, 15, 100, f);
				Spawner.spawned++;
				break;
			case 2:
				new EnemyTanks("enemy", 500, 880, 560, 940, Facing.UP, 15, 100, f);
				Spawner.spawned++;
				break;
			default:
				break;
			}
		}
		
	}
	
	
	
}
