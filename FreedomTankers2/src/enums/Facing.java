package enums;

/**
 * 
 * Freedom_tankers Facing.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         24 abr. 2020
 */
public enum Facing {

	//facing sides 
	UP, DOWN, LEFT, RIGHT
}
