package enums;

/**
 * FreedomTankers2
 * Stats.java
 * @author Alvaro Owen de la Quintana
 *
 * 27 may. 2020
 */
public enum StatsInfo {
	
	//kinds of stats
	any, hp, damage, xp, speed, enemies
}
