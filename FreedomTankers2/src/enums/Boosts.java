package enums;

/**
 * FreedomTankers2
 * Boosts.java
 * @author Alvaro Owen de la Quintana
 *
 * 17 may. 2020
 */
public enum Boosts {
	
	//boosts for the player
	hp, speed, damage
}
