package environmentalClases;

import java.util.Random;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import clases.Character;
import clases.EnemyTanks;
import clases.Spawner;
import enums.Boosts;

public class Perks extends PhysicBody {
	
	static Random r = new Random();

	//effect of the perk
	Boosts boost;
	
	/**
	 * Constructor for the perk class
	 * 
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param f    field where the sprite will be drawn
	 */
	public Perks( int x1, int y1, int x2, int y2, Field f) {
		super("perk", x1, y1, x2, y2, 0, "images/miscellaneous/perk.png", f);
		this.boost = ranboost();
		this.trigger = true;
		if (this.firstCollidesWithField() instanceof Perks) {
			this.delete();
		}
	}

	/**
	 * sets the boost for the perk
	 * @return
	 */
	private Boosts ranboost() {
		int rboost = r.nextInt(10);
			if (rboost < 3) {
				return Boosts.hp;
			}else if (rboost < 6) {
				return Boosts.speed;
			}else {
				return Boosts.damage;
			}
	}

	/**
	 * applies a perk to the character randomly it can be: more speed, more damage
	 * or heal to 100% effect
	 * @param character 
	 */
	public void effect(Character c) {
		if (this.boost == Boosts.speed) {
			c.speed++;
			c.lastSpeed++;
			c.bspeed++;
			Hud.gethud().speed.updatespeed();
		} else if (this.boost == Boosts.damage) {
			c.attack += 10;
			Hud.gethud().damage.updatedamage();
		} else if (this.boost == Boosts.hp) {
			c.hp = 100;
			Hud.gethud().health.updatehp();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
