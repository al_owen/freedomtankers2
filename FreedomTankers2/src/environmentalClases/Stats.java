package environmentalClases;

import Core.Field;
import Core.Sprite;
import clases.Character;
import clases.Spawner;
import enums.StatsInfo;

/**
 * FreedomTankers2 Stats.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         26 may. 2020
 */
public class Stats extends Sprite {

	/**
	 * constructor for the stats class
	 * 
	 * @param x1  The horizontal position in the field of the upper-left pixel in
	 *            the sprite
	 * @param y1  The vertical position in the field of the upper-left pixel in the
	 *            sprite
	 * @param f   field where the sprite will be drawn
	 * @param sti states the info
	 */
	public Stats(int x1, int y1, Field f, StatsInfo sti) {
		super("stats", x1, y1, x1, y1, 0, " ", f);
		this.solid = false;
		this.text = true;
		this.textColor = 0xFFFFFF;
		updatestats(sti);
	}

	/**
	 * states the info that will be shown
	 * @param sti
	 */
	private void updatestats(StatsInfo sti) {
		if (sti == StatsInfo.hp) {
			updatehp();
		} else if (sti == StatsInfo.speed) {
			updatespeed();
		} else if (sti == StatsInfo.damage) {
			updatedamage();
		} else if (sti == StatsInfo.xp) {
			updatexp();
		} else if (sti == StatsInfo.enemies) {
			updateenemies();
		} else {
			updateany("Your stats are: ");
		}

	}

	/**
	 * updates any kind of text
	 */
	public void updateany(String text) {
		this.path = text;
	}

	/**
	 * updates the enemies remaining
	 */
	public void updateenemies() {
		this.path = "Enemies remaining: " + Spawner.enemies;
	}

	/**
	 * updates the experience
	 */
	public void updatexp() {
		this.path = "XP earned: " + Character.getInstance().xp;
	}

	/**
	 * updates the damage of our tank
	 */
	public void updatedamage() {
		this.path = "Damage: " + Character.getInstance().attack;
	}

	/**
	 * updates the speed of our tank
	 */
	public void updatespeed() {
		this.path = "Speed: " + Character.getInstance().speed;
	}

	/**
	 * updates the hp of our tank
	 */
	public void updatehp() {
		this.path = "Health: " + Character.getInstance().hp;

	}

}
