package environmentalClases;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Forest extends PhysicBody {

	/**
	 * Constructor of Forest class
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 * @param f          field where the boss will be draw
	 */
	public Forest(int x1, int y1, int x2, int y2, String path, Field f) {
		super("forest", x1, y1, x2, y2, 0, path, f);
		this.trigger=true;
	}

	@Override
	public void onCollisionEnter(Sprite sp) {
		
	}

	
	@Override
	public void onCollisionExit(Sprite sp) {
		
	}

}
