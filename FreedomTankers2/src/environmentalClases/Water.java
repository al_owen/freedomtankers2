package environmentalClases;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

/**
 * 
 * Freedom_tankers
 * Water.java
 * @author Alvaro Owen de la Quintana
 *
 * 24 abr. 2020
 */
public class Water extends PhysicBody {
	/**
	 * Constructor for the water class
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 * @param f    field where the sprite will be drawn
	 */
	public Water(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.physicBody = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
