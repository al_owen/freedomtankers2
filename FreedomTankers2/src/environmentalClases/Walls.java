package environmentalClases;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import clases.Bullets;
import clases.Character;
import interfaces.Destructible;

public class Walls extends PhysicBody implements Destructible {

	/**
	 * Constructor of the wall class,
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 * @param f    field where the sprite will be drawn
	 */
	public Walls(int x1, int y1, int x2, int y2, Field f) {
		super("wall", x1, y1, x2, y2, 0, "images/enviroment/commonwall.png", f);
		this.trigger=false;
	}

	@Override
	public void destroy(Bullets b) {
		
		if (b.friendly) {
			Character.getInstance().xp+=2;
			Hud.gethud().xp.updatexp();
		}
		b.delete();
		this.delete();
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
