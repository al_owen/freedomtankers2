package environmentalClases;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import clases.Bullets;
import interfaces.Destructible;

/**
 * @author aldlq Alvaro Owen de la Quintana Freedom_tankers 7 feb. 2020
 */
public class Borders extends PhysicBody implements Destructible {

	/**
	 * This is the Border class, it creates non destructable objects that limits the
	 * player's movement
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 * @param f    field where the sprite will be drawn
	 */
	public Borders(String name, int x1, int y1, int x2, int y2, String path, Field f) {
		super(name, x1, y1, x2, y2, 0, path, f);
		this.physicBody = false;
		this.trigger = false;

	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy(Bullets b) {
		b.delete();
	}

}
