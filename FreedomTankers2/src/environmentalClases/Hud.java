package environmentalClases;

import Core.Field;
import enums.StatsInfo;

/**
 * FreedomTankers2 Hud.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         26 may. 2020
 */
public class Hud {

	// creates a singleton instance of hud
	private static Hud hud = null;

	// states the info that will be shown in the hud
	public Stats stats;
	public Stats health;
	public Stats damage;
	public Stats speed;
	public Stats xp;
	public Stats enemies;

	/**
	 * constructor of the hud class
	 * 
	 * @param x1 The horizontal position in the field of the upper-left pixel in the
	 *           sprite
	 * @param y1 The vertical position in the field of the upper-left pixel in the
	 *           sprite
	 * @param f  field where the sprite will be drawn
	 */
	private Hud(int x1, int y1, Field f) {
		this.stats = new Stats(x1, y1, f, StatsInfo.any);
		this.health = new Stats(x1, y1 + 50, f, StatsInfo.hp);
		this.damage = new Stats(x1, y1 + 100, f, StatsInfo.damage);
		this.speed = new Stats(x1, y1 + 150, f, StatsInfo.speed);
		this.xp = new Stats(x1, y1 + 200, f, StatsInfo.xp);
		this.enemies = new Stats(x1, y1 + 250, f, StatsInfo.enemies);
	}

	/**
	 * Instantiates the hud
	 * 
	 * @param x1 The horizontal position in the field of the upper-left pixel in the
	 *           sprite
	 * @param y1 The vertical position in the field of the upper-left pixel in the
	 *           sprite
	 * @param f  field where the sprite will be drawn
	 * @return hud
	 */
	public static Hud gethud(int x1, int y1, Field f) {
		if (hud == null) {
			hud = new Hud(x1, y1, f);
		}
		return hud;
	}

	/**
	 * return the hud
	 * 
	 * @return
	 */
	public static Hud gethud() {
		return hud;
	}

	/**
	 * restarts the hud in case the field has been cleared
	 * 
	 * @param f field where the sprite will be drawn
	 * @return
	 */
	public static Hud restarthud(Field f) {
		f.sprites.add(hud.stats);
		f.sprites.add(hud.health);
		Hud.gethud().health.updatehp();
		f.sprites.add(hud.damage);
		Hud.gethud().damage.updatedamage();
		f.sprites.add(hud.speed);
		Hud.gethud().speed.updatespeed();
		f.sprites.add(hud.xp);
		Hud.gethud().xp.updatexp();
		f.sprites.add(hud.enemies);
		Hud.gethud().enemies.updateenemies();
		return hud;
	}
}
