Once upon a time there was peace, we were all happy and free, our lord and savior Sam made sure of it. Sam is a bald eagle, most of us call him "uncle Sam" because he is always taking care of us, like an uncle. When he was younger, he had a nice blonde toupee (he wasn't bald yet), but more important he had a dream of a free society free from bigotry, free from bad people. All that dream was shattered when a commie capybara tried to implement his communist regime, he threatens our freedom and we will not allow it. We will fight for our freedom we are Freedom Fighters; we are Freedom Tankers.
This is a game inspired in Battle city
The game is written in java and still in development.
Basic input:


"e" - select the option (only in the main menu)

"+" - increases the dificulty (only in the main menu - it's a hidden feature)

"-" - decreases the dificulty (only in the main menu - it's a hidden feature) 

"w" - goes up

"s" - goes down

"a" - goes left

"d" - goes right

"spacebar" - shoot

The game itself consists of two stages and the main menu. First, you must open the "MainMenu" and then select one of three different options.
The first option is Level 1, the second is Level 2 and the last one is exit.
Exit is pretty self-explanatory; the window is closed and the music stopped.
Level 1 is the first level and as such should be easy enough, it has many walls, which protects our tank from enemy attacks.
Our main objective is to destroy the enemies
Level 2 is the second level, your objective is to destroy all enemies. This level should be harder as there is less protection for your tank.
Level 3 is the final level, here you will face the Nage Fahrer, you still have enemies spawning everywhere but your objective is to defeat the Fahrer if you fail, you lose... even if you destroy all enemy tanks.

Defeat our enemy Freedom fighter and bring our Freedom back... 